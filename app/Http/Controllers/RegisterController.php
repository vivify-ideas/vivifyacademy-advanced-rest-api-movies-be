<?php

namespace App\Http\Controllers;

use App\Http\Requests\Register;
use App\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{

    public function register(Register $request){
        $user = User::create([
        	'name' => $request->name,
        	'email' => $request->email,
        	'password' => bcrypt($request->password)
    	]);
    	return $user;
    }
}
?>